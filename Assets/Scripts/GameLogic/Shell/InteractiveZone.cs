﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using HPEW.Input;
using Unity.Jobs;

namespace HPEW.Interactive
{
    public class InteractiveZone : MonoBehaviour
    {
        [Inject] InputSystem _inputSystem;

        Queue<Let> lets;

        void Awake()
        {
            SimpleJob job = new SimpleJob();
            job.Schedule().Complete();
            
            lets = new Queue<Let>();

            _inputSystem.OnTouch += type => 
            {
                if (type == Input.TouchType.OneTouch) 
                    TouchHandler(); 
            };

            _inputSystem.OnSwipe += SwipeHandler;
        }

        private void OnTriggerEnter(Collider other)
        {
            var let = other.GetComponent<Let>();

            if (let == null) return;
            lets.Enqueue(let);
        }

        private void TouchHandler()
        {
            if (lets.Count != 0) 
            {
                var let = lets.Peek();
                

                if (let is TouchLet || let is DestructibleLet)
                {
                    let.Interact();

                    if (!let.isInteractive)
                        lets.Dequeue();
                }
            }
        }

        private void SwipeHandler(TypeSwipe type)
        {
            if (lets.Count != 0 && lets.Peek() is SwipeLet sLet)
            {
                sLet.Interact(type);

                if (!sLet.isInteractive)
                    lets.Dequeue();
            }
        }
    }
}

public struct SimpleJob : IJob
{
    public void Execute()
    {
    }
}