﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;

namespace HPEW.ShellSystem
{
    [RequireComponent(typeof(Rigidbody))]
    public class ShellMotionController : MonoBehaviour
    {
        [Header("Options for animation of a shot")]

        [SerializeField, Range(0f, 100f)]
        float startSpeed = 10.0f;
        public float StartSpeed => startSpeed;

        [SerializeField, Range(0f, 100f)]
        float targetSpeed = 10.0f;
        public float TargetSpeed => targetSpeed;

        [SerializeField, Range(0f, 100f)]
        float maxAccelerationAnim = 10f;
        public float MaxAccelerationAnim => maxAccelerationAnim;


        [Header("Options for gameplay move")]

        [SerializeField, Range(0f, 100f)]
        float limitSpeed = 10.0f;
        public float LimitSpeed => limitSpeed;

        [SerializeField, Range(0f, 100f)]
        float maxAcceleration = 10f;
        public float MaxAcceleration => maxAcceleration;


        Rigidbody _rb;
        Collider _collider;

        [SerializeField] Vector3 desiredVelocity; //TODO: Serialize for test

        public Vector3 CurrentVelocity => _rb.velocity;

        IDisposable fixedUpdateDispose;

        private void Awake()
        {
            _rb = GetComponent<Rigidbody>();
            _collider = GetComponent<Collider>();
        }

        public void StartMoving()
        {
            StartShotAnimation();
        }

        void StartShotAnimation()
        {
            fixedUpdateDispose?.Dispose();
            desiredVelocity = transform.forward * targetSpeed;

            fixedUpdateDispose = Observable.EveryFixedUpdate()
                .Subscribe(index =>
                {
                    if (index == 0) SetVelocity(transform.forward * startSpeed, maxAccelerationAnim);
                    else SetVelocity(_rb.velocity, maxAccelerationAnim);

                    if (_rb.velocity == desiredVelocity)
                        StartGamePlayMoving();
                    
                        
                });
        }

        void StartGamePlayMoving()
        {
            fixedUpdateDispose?.Dispose();

            desiredVelocity = transform.forward * LimitSpeed;

            fixedUpdateDispose = Observable.EveryFixedUpdate()
                .Subscribe(delegate { SetVelocity(_rb.velocity, maxAcceleration); });
        }

        private void SetVelocity(Vector3 currentVelocity, float acceleration)
        {
            var xAxis = Vector3.right;
            var zAxis = Vector3.forward;

            var currentX = currentVelocity.x;
            var currentZ = currentVelocity.z;

            var maxSpeedChange = acceleration * Time.deltaTime;

            var newX = Mathf.MoveTowards(currentX, desiredVelocity.x, maxSpeedChange);
            var newZ = Mathf.MoveTowards(currentZ, desiredVelocity.z, maxSpeedChange);

            currentVelocity += xAxis * (newX - currentX) + zAxis * (newZ - currentZ);

            _rb.velocity = currentVelocity;
        }
    }
}