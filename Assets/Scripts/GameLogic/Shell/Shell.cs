﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using UniRx;
using HPEW.Input;
using HPEW.Systemic;

namespace HPEW.ShellSystem
{
    [RequireComponent(typeof(ShellMotionController))]
    public class Shell : MonoBehaviour
    {
        [Inject] ShellMotionController _motionController;
        [Inject] Settings _settings;

        private void Awake()
        {
            Observable.Timer(TimeSpan.FromSeconds(_settings.ShotTime))
                .Subscribe((Action<long>)delegate
                {
                    _motionController.StartMoving();
                });
        }
    }
}
