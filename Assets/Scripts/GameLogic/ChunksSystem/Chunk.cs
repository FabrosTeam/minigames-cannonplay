﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
using Zenject;
using UniRx;
using HPEW.Systemic;
using HPEW.Interactive;
using HPEW.Pool;
using HPEW.ShadersAPI;

namespace HPEW.ChunkSystem
{
    public class Chunk : MonoBehaviour
    {
        [SerializeField] protected Transform _begin;
        [SerializeField] protected Transform _end;
        public Transform Begin => _begin;
        public Transform End => _end;


        [Inject] protected Settings _settings;
        [Inject] protected DiContainer _container;
        [Inject] PoolLet[] _poolLets;

        List<Let> lets;

        int interval;

        DissolveAPI _dissolve;
        float speedDissolve;

        protected virtual void Awake()
        {
            _dissolve = GetComponent<DissolveAPI>();

            interval = _settings.IntervalLets;
            speedDissolve = _settings.SpeedDissolveChunk;

            lets = new List<Let>();

            Create();
        }

        protected virtual void Start()
        {
            _dissolve.ChangeDissolveValue = 1f;
            _dissolve.StartAnimationAppear(speedDissolve);
        }

        protected virtual void Create()
        {
            for (int i = -50; i < 50; i += interval) 
            {
                var indexPool = Random.Range(0, _poolLets.Length);

                var let = _poolLets[indexPool].Spawn();

                var parent = let.transform.parent;
                let.transform.SetParent(transform);
                let.transform.localPosition = new Vector3(0, 2.5f, i);

                lets.Add(let);

                Action handler = null;
                handler = delegate
                {
                    let.OnDestroyed -= handler;

                    Observable.Timer(TimeSpan.FromSeconds(2.5f))
                    .Subscribe(delegate
                    {
                        lets.Remove(let);
                        _poolLets[indexPool].Despawn(let);
                    });
                };

                let.OnDestroyed += handler;
            }
        }
    }
}