﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using UniRx;
using HPEW.Systemic;
using HPEW.ShellSystem;
using HPEW.Zenject;

namespace HPEW.ChunkSystem
{
    public class ChunksPlacer
    {
        Chunk[] _chunkPrefabs;

        List<Chunk> _spawnedChunks;

        Transform _player;

        int countInitChunks = 0; 
        int maxCountChunks = 1;

        DiContainer _container;

        public ChunksPlacer(DiContainer container, Assets assets, Settings settings, Shell shell)
        {
            _container = container;
            _chunkPrefabs = assets.ChunkPrefabs;
            _player = shell.transform;

            countInitChunks = settings.CountInitChunks;
            maxCountChunks = settings.MaxCountChunks;

            if (countInitChunks + 1 > maxCountChunks)
            {
                Debug.LogError("Count Init Chunks + start chunk > max Count Chunks");
                return;
            }

            if (countInitChunks == 0)
            {
                Debug.LogError("CountInitChunks it`s 0!");
                return;
            }

            _spawnedChunks = new List<Chunk>();
            
            var firstChunk = Object.FindObjectOfType<Chunk>();

            if (firstChunk == null)
            {
                Debug.LogError("No initial chunk on stage");
                return;
            }

            _spawnedChunks.Add(Object.FindObjectOfType<Chunk>());

            for (int i = 0; i < countInitChunks; i++)
            {
                CreateChunk();
            }

            Observable.EveryUpdate()
                .Subscribe(delegate
                {
                    if (_player.position.z > _spawnedChunks[1].End.position.z)
                    {
                        CreateChunk();
                    }

                });
        }


        private void CreateChunk()
        {
            var newChunk = _container.InstantiatePrefab(_chunkPrefabs[Random.Range(0, _chunkPrefabs.Length)])
                .GetComponent<Chunk>();

            newChunk.transform.position = _spawnedChunks[_spawnedChunks.Count - 1].End.position - newChunk.Begin.localPosition;

            _spawnedChunks.Add(newChunk);

            newChunk.gameObject.name = "Chunk";

            if (_spawnedChunks.Count > maxCountChunks)
            {
                Object.Destroy(_spawnedChunks[0].gameObject);
                _spawnedChunks.RemoveAt(0);
            }
                
        }
    }
}