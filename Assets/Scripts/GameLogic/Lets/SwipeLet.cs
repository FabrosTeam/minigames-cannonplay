﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using Zenject;
using UniRx;
using HPEW.Input;
using HPEW.ShadersAPI;
using HPEW.Systemic;

namespace HPEW.Interactive
{

    public class SwipeLet : Let
    {
        [SerializeField] float distance = 5f;
        Vector3 direction = Vector3.zero;

        [SerializeField] float speed = 10f;

        float speedDissolve;

        Image _arrow;
        TypeSwipe reference;

        DissolveAPI _dissolve;

        [Inject] Settings _settings;

        protected override void Awake()
        {
            base.Awake();
            _arrow = GetComponentInChildren<Image>();
            _dissolve = GetComponentInChildren<DissolveAPI>(true);

            speedDissolve = _settings.SpeedDissolveSwipeLet;
        }


        public override void Restart()
        {
            var enums = Enum.GetValues(typeof(TypeSwipe));

            reference = (TypeSwipe)enums.GetValue(Random.Range(0, enums.Length));
            SetDirectionArrow(reference);

            isInteractive = true;

            _dissolve.ChangeDissolveValue = 1;
            _dissolve.StartAnimationAppear(speedDissolve);
        }

        public override void Sleep()
        {

        }


        public override void Interact(object data = null)
        {
            var typeSwipe = (TypeSwipe)data;
            if (typeSwipe != reference) return;

            isInteractive = false;
            InvokeOnDestroyed();
            Observable.FromMicroCoroutine(_ => Move())
                .Subscribe();
        }

        private IEnumerator Move()
        {
            float t = 0;

            var start = transform.position;
            var end = transform.position + direction * distance;

            while (transform.position != end)
            {
                transform.position = Vector3.Lerp(start, end, t);

                t += Time.deltaTime * speed;

                yield return null;
            }
        }

        private void SetDirectionArrow(TypeSwipe type)
        {
            var rotation = _arrow.transform.localRotation;

            switch (type)
            {
                case TypeSwipe.Left:
                    rotation = Quaternion.Euler(0, 0, -90);
                    direction = -transform.right;
                    break;
                case TypeSwipe.Right:
                    rotation = Quaternion.Euler(0, 0, 90);
                    direction = transform.right;
                    break;
                case TypeSwipe.Up:
                    rotation = Quaternion.Euler(0, 0, 180);
                    direction = transform.up;
                    break;
                case TypeSwipe.Down:
                    rotation = Quaternion.Euler(0, 0, 0);
                    direction = -transform.up;
                    break;
            }

            _arrow.transform.localRotation = rotation;
        }

    }
}
