﻿using UnityEngine;
using HPEW.ShellSystem;
using Zenject;
using UniRx;
using System;

namespace HPEW.ChunkSystem
{
    [RequireComponent(typeof(Camera))]
    public class CameraBehaviour : MonoBehaviour
    {
        [SerializeField, Range(0f, 1f)]
        float smoothTime = 0.5f;

        [SerializeField, Range(0f, 100f)]
        float activationDistance = 10f;
        Vector3 offset;
        Vector3 velocity = Vector3.zero;

        [Inject] Shell _shell;
        Transform _focus;

        private void Awake()
        {
            _focus = _shell.transform;
            offset = transform.position - _focus.position;
        }

        private void LateUpdate()
        {
            var desiredPosition = _focus.position + offset;
            var smoothedPosition = Vector3.SmoothDamp(transform.position, desiredPosition, ref velocity, smoothTime);
            transform.position = smoothedPosition;
        }


    }
}
