﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace HPEW.Input
{

    public class TouchManager
    {
        Action<TouchType> action;

        IDisposable disposable;

        public TouchManager(Action<TouchType> action)
        {
            this.action = action;

            disposable = Observable.EveryUpdate().Subscribe(_ => Update());
        }

        void Update()
        {
#if UNITY_ANDROID
            if (UnityEngine.Input.touchCount > 0 && UnityEngine.Input.GetTouch(0).phase == TouchPhase.Began)
                action?.Invoke(TouchType.OneTouch);
#endif

#if UNITY_EDITOR
            if (UnityEngine.Input.GetMouseButtonDown(0))
                action?.Invoke(TouchType.OneTouch);
#endif
        }
    }

    public enum TouchType
    {
        OneTouch,
        DoubleTouch
    }
}
