﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace HPEW.ShadersAPI
{
    public class DissolveAPI : MonoBehaviour
    {
        public event Action OnDisappear = delegate { };
        public event Action OnAppear = delegate { };
        Material materialG = null;

        /// <summary>
        /// Метод изменяет значение DissolveAmount от 0 до 1
        /// </summary>
        /// <param name="value"></param>
        public float ChangeDissolveValue
        {
            set
            {
                if (value < 0) materialG.SetFloat("_DissolveAmount", 0);
                else if(value > 1) materialG.SetFloat("_DissolveAmount", 1);
                else materialG.SetFloat("_DissolveAmount", value);
            }
        }

        private void Awake()
        {
            materialG = gameObject.GetComponent<Renderer>().material;
        }


        /// <summary>
        /// метод запускает анимацию появления
        /// </summary>
        /// <param name="speed">скорость появления (рекомендуемая скорость 0.01f)</param>
        public void StartAnimationAppear(float speed)
        {
            StartCoroutine(AnimationAppeare(speed));
        }

        IEnumerator AnimationDisappeare(float speed)
        {
            while (materialG.GetFloat("_DissolveAmount") < 1)
            {
                materialG.SetFloat("_DissolveAmount", materialG.GetFloat("_DissolveAmount") + speed);
                if (materialG.GetFloat("_DissolveAmount") > 1)
                {
                    materialG.SetFloat("_DissolveAmount", 1);
                }
                yield return null;
            }

            OnDisappear();
        }

        IEnumerator AnimationAppeare(float speed)
        {
           
            while (materialG.GetFloat("_DissolveAmount") > 0)
            {

                materialG.SetFloat("_DissolveAmount", materialG.GetFloat("_DissolveAmount") - speed);
                if (materialG.GetFloat("_DissolveAmount") < 0)
                {
                    materialG.SetFloat("_DissolveAmount", 0);
                }
                yield return null;
            }

            OnAppear();
        }


        /// <summary>
        /// метод запускает анимацию исчезновения
        /// </summary>
        /// <param name="speed">скорость исчезновения (рекомендуемая скорость 0.01f)</param>
        public void StartAnimationDisappear(float speed)
        {
            StartCoroutine(AnimationDisappeare(speed));
        }
    }
}
