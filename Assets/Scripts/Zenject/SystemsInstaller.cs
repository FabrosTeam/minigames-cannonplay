﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using HPEW.ChunkSystem;
using HPEW.Pool;
using HPEW.Interactive;

namespace HPEW.Zenject
{
    public class SystemsInstaller : MonoInstaller
    {
        [Inject] Systemic.Assets _assets;
        [Inject] Systemic.Settings _settings;

        public override void InstallBindings()
        {
            Container.Bind<ChunksPlacer>()
                .AsSingle()
                .NonLazy();

            var lets = _assets.LetPrefabs;

            foreach (var prefab in lets)
            {
                Container.BindMemoryPool<Let, PoolLet>()
                    .WithInitialSize(_settings.InitialSize)
                    .FromComponentInNewPrefab(prefab)
                    .UnderTransformGroup("PoolContainer - " + prefab.name);
            }
        }
    }
}
