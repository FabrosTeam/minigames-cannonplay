﻿using UnityEngine;
using Zenject;
using HPEW.Interactive;


namespace HPEW.Pool
{
    public class PoolLet : MonoMemoryPool<Let>
    {
        protected override void Reinitialize(Let let)
        {
            let.Restart();
        }

        protected override void OnDespawned(Let let)
        {
            base.OnDespawned(let);
            let.Sleep();
        }
    }
}