﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HPEW.Math
{
    public static class Geometry
    {
        public static Vector3 RotateVector(Vector3 vector, float angle, Axis axis)
        {
            var turnMatrix = new float[3, 3];

            angle = angle * Mathf.PI / 180;

            switch (axis)
            {
                case Axis.Z:
                    turnMatrix[0, 0] = Mathf.Cos(angle);
                    turnMatrix[0, 1] = -Mathf.Sin(angle);
                    turnMatrix[0, 2] = 0;

                    turnMatrix[1, 0] = Mathf.Sin(angle);
                    turnMatrix[1, 1] = Mathf.Cos(angle);
                    turnMatrix[1, 2] = 0;

                    turnMatrix[2, 0] = 0;
                    turnMatrix[2, 1] = 0;
                    turnMatrix[2, 2] = 1;
                    break;
                case Axis.Y:
                    turnMatrix[0, 0] = Mathf.Cos(angle);
                    turnMatrix[0, 1] = 0;
                    turnMatrix[0, 2] = Mathf.Sin(angle);

                    turnMatrix[1, 0] = 0;
                    turnMatrix[1, 1] = 1;
                    turnMatrix[1, 2] = 0;

                    turnMatrix[2, 0] = -Mathf.Sin(angle);
                    turnMatrix[2, 1] = 0;
                    turnMatrix[2, 2] = Mathf.Cos(angle);
                    break;
                case Axis.X:
                    turnMatrix[0, 0] = 1;
                    turnMatrix[0, 1] = 0;
                    turnMatrix[0, 2] = 0;

                    turnMatrix[1, 0] = 0;
                    turnMatrix[1, 1] = Mathf.Cos(angle);
                    turnMatrix[1, 2] = -Mathf.Sin(angle);

                    turnMatrix[2, 0] = 0;
                    turnMatrix[2, 1] = Mathf.Sin(angle);
                    turnMatrix[2, 2] = Mathf.Cos(angle);
                    break;
            }

            var inMatrix = new float[]
            {
                vector.x, vector.y, vector.z
            };

            var outMatrix = new float[]
            {
                turnMatrix[0,0] * inMatrix[0] + turnMatrix[0,1] * inMatrix[1] + turnMatrix[0,2] * inMatrix[2],
                turnMatrix[1,0] * inMatrix[0] + turnMatrix[1,1] * inMatrix[1] + turnMatrix[1,2] * inMatrix[2],
                turnMatrix[2,0] * inMatrix[0] + turnMatrix[2,1] * inMatrix[1] + turnMatrix[2,2] * inMatrix[2],
            };

            return new Vector3(outMatrix[0], outMatrix[1], outMatrix[2]);
        }
    }

    public enum Axis
    {
        X,
        Y,
        Z
    }
}